const express = require('express');
const router = express.Router();

//Ruta Postulante
const clienteRouter = require('./cliente');
const productoRouter = require('./producto');




//MiddleWare para usar Rutas Hijas
router.use("/cliente",clienteRouter);


router.use("/producto",productoRouter)



module.exports = router;


