const express = require('express');

const productoRouter = express.Router();

const {obtenerProductos} = require('../controllers/productoController');
productoRouter.get('/', obtenerProductos)



module.exports = productoRouter;