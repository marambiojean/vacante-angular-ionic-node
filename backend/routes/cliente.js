const express = require('express');

const clienteRouter = express.Router();

const {obtenerClientes, agregarSaldo, comprarProducto, verHistorial, editarCliente, verRecargas} = require('../controllers/clienteController');
clienteRouter.get('/', obtenerClientes)
clienteRouter.post('/', agregarSaldo)
clienteRouter.get('/:id', verHistorial)
clienteRouter.get('/recargas/:id', verRecargas)
clienteRouter.put('/', editarCliente)





clienteRouter.post('/compra', comprarProducto)




module.exports = clienteRouter;