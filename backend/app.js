
const express = require('express');
const path = require('path');

const dotenv = require('dotenv');
const router = require('./routes/router');

const cors = require('cors');


dotenv.config({path: './.env'})

//Inicializo mi app Express
const app = express();
app.use(express.urlencoded({extended:false}));
app.use(express(JSON));
app.use(express.text());
app.use(express.json());
app.use(cors());


//Router para mis EndPoints
app.use(router);


const PORT = 3000;

app.listen(PORT, ()=>{
    console.log('server escuchando');
})

