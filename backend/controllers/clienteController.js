const conexion = require('../database/conexion')


const obtenerClientes = (req,res) => {
    conexion.query(`SELECT * FROM cliente`, (error, results)=>{
        if(error){
            console.log(error);
        }
        else{
            res.json(results)
        }
    });
}

const agregarSaldo = (req,res) => {


    console.log(req.body);
    const id = req.body.id
    const monto = req.body.monto
    console.log(id,monto);
    conexion.query(`
        INSERT 
            INTO recarga (monto, cliente_id)
            VALUES (${monto}, ${id});
        `, (error, results)=>{
        if(error){
            console.log(error);
        }
        else{
            res.json(results)
        }
    });

}

const comprarProducto = (req,res) => {
    const cliente_id = req.body.cliente_id
    const producto_id = req.body.producto_id
    console.log("hola")
    conexion.query(`INSERT 
        INTO historial (cliente_id,producto_id)
        VALUES (${cliente_id}, ${producto_id});
        `, (error, results)=>{
        if(error){
            console.log(error);
        }
        else{
            res.json({status: 'Compra Exitosa'})
        }
    });
}

const editarCliente = (req,res) => {
    const cliente_id = req.body.cliente_id
    const nombre = req.body.nombre

    conexion.query(`
        UPDATE cliente
    SET nombre = "${nombre}"
    WHERE id = ${cliente_id};
    
    `, (error, results)=>{
        if(error){
            console.log(error);
        }
        else{
            res.json(results)
        }
    });
}

const verHistorial = (req,res) => {
    let cliente_id = req.params.id
    conexion.query(`
        SELECT 
            p.nombre,
            p.precio
        FROM historial as h
        INNER JOIN cliente as c ON c.id = h.cliente_id
        INNER JOIN producto as p ON p.id = h.producto_id
        WHERE h.cliente_id = ${cliente_id}
    `, (error, results)=>{
        if(error){
            console.log(error);
        }
        else{
            res.json(results)
        }
    });
}

const verRecargas = (req,res) => {
    let cliente_id = req.params.id
    conexion.query(`
    SELECT 
        c.nombre as nombre,
        r.monto as monto
    FROM recarga as r
    INNER JOIN cliente as c ON c.id = r.cliente_id
    WHERE r.cliente_id = ${cliente_id}
    `, (error, results)=>{
        if(error){
            console.log(error);
        }
        else{
            res.json(results)
        }
    });
}

module.exports = {
    obtenerClientes,
    agregarSaldo,
    comprarProducto,
    verHistorial,
    editarCliente,
    verRecargas
};