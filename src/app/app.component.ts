import { Component } from '@angular/core';
@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent {
  public appPages = [
    { title: 'Agregar Saldo', url: '/agregar-saldo', icon: 'mail' },
    { title: 'Comprar', url: '/comprar', icon: 'paper-plane' },
    { title: 'Historial', url: '/historial', icon: 'heart' },
    { title: 'Editar Cliente', url: '/editar', icon: 'archive' },
  ];
  public labels = ['Family', 'Friends', 'Notes', 'Work', 'Travel', 'Reminders'];
  constructor() {}
}
