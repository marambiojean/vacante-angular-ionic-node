import { Component, OnInit } from '@angular/core';
import { Compra } from '../interfaces/compra';
import { ServiceCliente } from '../services/servicecliente.service';

@Component({
  selector: 'app-comprar',
  templateUrl: './comprar.page.html',
  styleUrls: ['./comprar.page.scss'],
})
export class ComprarPage implements OnInit {
  public folder: string;

  productos:any = []

  compra:Compra = {
    cliente_id: '1',
    producto_id: 0
  }

  constructor(private serviceCliente:ServiceCliente) { }

  ngOnInit() {
    this.obtenerProductos()
  }

  obtenerProductos(){
    this.serviceCliente.obtenerProductos()
      .subscribe((res)=>{
        this.productos = res
      })
  }

  comprarProducto(producto_id:number){

    this.compra = {
      cliente_id: '1',
      producto_id: producto_id
    }

    this.serviceCliente.comprarProducto(this.compra)
      .subscribe()
  }
}
