import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http'
import { Recarga } from '../interfaces/recarga';
import { Compra } from '../interfaces/compra';
import { Cliente } from '../interfaces/cliente';

@Injectable({
  providedIn: 'root'
})
export class ServiceCliente{

  constructor(private http:HttpClient) { }

  obtenerClientes(){
    return this.http.get('http://localhost:3000/cliente')
  }

  agregarSaldo(recarga:Recarga){
    return this.http.post('http://localhost:3000/cliente',recarga)
  }

  obtenerProductos(){
    return this.http.get('http://localhost:3000/producto')
  }

  comprarProducto(compra:Compra){
    return this.http.post('http://localhost:3000/cliente/compra', compra)
  }

  verHistorial(cliente_id:number){
    return this.http.get('http://localhost:3000/cliente/'+cliente_id)
  }

  verRecargas(cliente_id:number){
    return this.http.get('http://localhost:3000/cliente/recargas/'+cliente_id)
  }

  editarCliente(cliente:Cliente){
    return this.http.put('http://localhost:3000/cliente',cliente)
  }


}
