import { Component, OnInit } from '@angular/core';
import { ServiceCliente } from '../services/servicecliente.service';
import { Cliente } from '../interfaces/cliente';

@Component({
  selector: 'app-editar',
  templateUrl: './editar.page.html',
  styleUrls: ['./editar.page.scss'],
})
export class EditarPage implements OnInit {

  nombre:string = ''
  

  clientes:any = []

  cliente:Cliente = {
    cliente_id: '0',
    nombre: ''
  }



  
  constructor(private serviceCliente:ServiceCliente) { }

  ngOnInit() {
    this.obtenerClientes()

  }

  obtenerClientes(){
    this.serviceCliente.obtenerClientes().
      subscribe( 
        (res) => {
          this.clientes = res
        }
      )

    
  }

  cambiarNombre(){
    this.cliente={
      cliente_id: '0',
      nombre: this.nombre
    }
    this.serviceCliente.editarCliente(this.cliente)
      .subscribe()
  }

}
