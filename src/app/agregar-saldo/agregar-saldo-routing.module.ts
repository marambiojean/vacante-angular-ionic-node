import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AgregarSaldoPage } from './agregar-saldo.page';

const routes: Routes = [
  {
    path: '',
    component: AgregarSaldoPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AgregarSaldoPageRoutingModule {}
