import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AgregarSaldoPageRoutingModule } from './agregar-saldo-routing.module';

import { AgregarSaldoPage } from './agregar-saldo.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AgregarSaldoPageRoutingModule,
    FormsModule
  ],
  declarations: [AgregarSaldoPage]
})
export class AgregarSaldoPageModule {}
