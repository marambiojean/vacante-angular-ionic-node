import { Component, OnInit } from '@angular/core';
import { ServiceCliente } from '../services/servicecliente.service';
import { Recarga } from '../interfaces/recarga';



@Component({
  selector: 'app-agregar-saldo',
  templateUrl: './agregar-saldo.page.html',
  styleUrls: ['./agregar-saldo.page.scss'],
})
export class AgregarSaldoPage implements OnInit {

  clientes:any = [];

  monto:number;
  id_cliente:string;

  recarga:Recarga = {
    id: null,
    monto: null
  }

  constructor(private serviceCliente:ServiceCliente) { }

  ngOnInit() {
    this.obtenerClientes()

  }

  obtenerClientes(){
    this.serviceCliente.obtenerClientes().
      subscribe( 
        (res) => {
          this.clientes = res
        }
      )

    
  }

  agregarSaldo(){
    this.recarga= {
      id: '1',
      monto: this.monto
    }


    this.serviceCliente.agregarSaldo(this.recarga)
      .subscribe()
  }

}
