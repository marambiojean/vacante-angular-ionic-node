import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ServiceCliente } from '../services/servicecliente.service';

@Component({
  selector: 'app-historial',
  templateUrl: './historial.page.html',
  styleUrls: ['./historial.page.scss'],
})
export class HistorialPage implements OnInit {


  compras: any = []

  recargas: any = []

  constructor(private serviceCliente:ServiceCliente){}



  async ngOnInit() {
    this.verHistorial()
    this.verRecargas()
  }

  verHistorial(){
    this.serviceCliente.verHistorial(1)
      .subscribe((res) => {
        this.compras = res
      })
  }

  verRecargas(){
    this.serviceCliente.verRecargas(1)
      .subscribe((res) => {
        this.recargas = res
      })
  }

}
